Date and Time Objects for GNU Octave
====================================

This project is a work in progress to provide modern, full-featured date and
time objects in the Octave scripting language for inclusion in GNU Octave.

Overview
--------

The scope of the project includes three class types

  * `datetime` - a class representing a specific point in time
  * `duration` - a class representing a time duration or time span
  * `calendarDuration` - a class representing a calendar duration

All of these types may be scalars or concatenated into arrays. Arithmetic
operations and other conversion methods are defined between them.

Usage
-----

To get started with this project very quickly, start Octave and run

    >> pkg install https://gitlab.com/mtmiller/octave-datetime/-/archive/master/octave-datetime-master.tar.gz
    >> pkg load datetime

Contributing
------------

If you would like to help make this project better, your contributions are
very welcome! Please take a look at [CONTRIBUTING.md](CONTRIBUTING.md).

License
-------

The functions in this project are licensed under the
[GPL](https://www.gnu.org/licenses/gpl.html), version 3 or later. See
[COPYING](COPYING) for the full license text.
