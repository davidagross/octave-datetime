## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-

function disp (self)

  switch (self.Format)
    case "y"
      v = self.m_milliseconds / (1000 * 60 * 60 * 24 * 365.2425);
      s = num2str (v, "   %g yrs");
    case "d"
      v = self.m_milliseconds / (1000 * 60 * 60 * 24);
      s = num2str (v, "   %g days");
    case "h"
      v = self.m_milliseconds / (1000 * 60 * 60);
      s = num2str (v, "   %g hr");
    case "m"
      v = self.m_milliseconds / (1000 * 60);
      s = num2str (v, "   %g min");
    case "s"
      v = self.m_milliseconds / 1000;
      s = num2str (v, "   %g sec");
    otherwise
      ms = self.m_milliseconds;
      dd = fix (ms / (1000 * 60 * 60 * 24));
      ms -= dd * 1000 * 60 * 60 * 24;
      hh = fix (ms / (1000 * 60 * 60));
      ms -= hh * 1000 * 60 * 60;
      mm = fix (ms / (1000 * 60));
      ms -= mm * 1000 * 60;
      ss = fix (ms / 1000);
      ms -= ss * 1000;
      s = cellfun (@(h, m, s) sprintf ("   %02d:%02d:%02d", h, m, s),
                   num2cell (hh), num2cell (mm), num2cell (ss),
                   "uniformoutput", false);
      s = cell2mat (s);
  endswitch

  disp (s);

endfunction
