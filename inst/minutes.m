## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} minutes (@var{x})
## @seealso{days, duration, hours, milliseconds, seconds, years}
## @end deftypefn

function y = minutes (x)

  if (nargin != 1)
    print_usage ();
  endif

  if (! isnumeric (x))
    error ("minutes: X must be a numeric array or a duration array");
  endif

  if (isnumeric (x))
    y = duration (0, x, 0, "Format", "m");
  endif

endfunction

%!assert (isobject (minutes (0)))
%!assert (isa (minutes (0), "duration"))
%!assert (size (minutes (0)), [1, 1])
%!assert (minutes (minutes (1:10)), 1:10)

## Test input validation
%!error minutes ()
%!error minutes (1, 2)
%!error minutes ("s")
