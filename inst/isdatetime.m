## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} isdatetime (@var{x})
## @seealso{datetime, duration, isduration}
## @end deftypefn

function ret = isdatetime (x)

  if (nargin != 1)
    print_usage ();
  endif

  ret = (isobject (x) && isa (x, "datetime"));

endfunction

%!assert (isdatetime ([]), false)
%!assert (isdatetime (1), false)
%!assert (isdatetime (1:10), false)
%!assert (isdatetime (hours (1)), false)
%!assert (isdatetime (datetime ()), true)

## Test input validation
%!error isdatetime ()
%!error isdatetime (1, 2)
